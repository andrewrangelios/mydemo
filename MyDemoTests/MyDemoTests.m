//
//  MyDemoTests.m
//  MyDemoTests
//
//  Created by Austin Burrow on 7/25/13.
//  Copyright (c) 2013 Intouch Solutions. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MyDemoTests : XCTestCase

@end

@implementation MyDemoTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
