//
//  AlertViewController.m
//  MyDemo
//
//  Created by Austin Burrow on 7/26/13.
//  Copyright (c) 2013 Intouch Solutions. All rights reserved.
//

#import "AlertViewController.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

@synthesize okButton;

-(void)viewDidAppear:(BOOL)animated {
    // Added an Alert
    [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"You have been alerted" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (IBAction)ok:(id)sender {
    // Go back to the root view
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
