//
//  AlertViewController.h
//  MyDemo
//
//  Created by Austin Burrow on 7/26/13.
//  Copyright (c) 2013 Intouch Solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIButton *okButton;

- (IBAction)ok:(id)sender;

@end
